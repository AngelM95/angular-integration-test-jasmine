import { HighlightDirective } from "./highlight.directive";
import { Component, DebugElement } from "@angular/core";
import { ComponentFixture, TestBed, async } from "@angular/core/testing";

import { By } from "@angular/platform-browser";

// Create Fake component to test the directive
@Component({
  template: `
    <p highlight="cyan">First</p>
    <p highlight>Second</p>
  `
})
class DirectiveHostComponent {}

describe("HighlightDirective", () => {
  let fixture: ComponentFixture<DirectiveHostComponent>;
  let debugerElements: DebugElement[];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DirectiveHostComponent, HighlightDirective]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectiveHostComponent);
    fixture.detectChanges();
  });

  it("should set default color to paragraph", () => {
    debugerElements = fixture.debugElement.queryAll(
      By.directive(HighlightDirective)
    );
    let de = debugerElements[1];
    let directive: HighlightDirective = de.injector.get(HighlightDirective);
    expect(de.nativeElement.style.backgroundColor).toBe(directive.bgColor);
  });

  it("should set cyan color to paragraph", () => {
    debugerElements = fixture.debugElement.queryAll(
      By.directive(HighlightDirective)
    );
    let de = debugerElements[0];
    expect(de.nativeElement.style.backgroundColor).toBe("cyan");
  });
});
