import { TestBed, async, ComponentFixture } from "@angular/core/testing";
import { AppComponent } from "./app.component";
import { By } from "@angular/platform-browser";
import {
  Component,
  Input,
  HostListener,
  DebugElement,
  Directive,
  NO_ERRORS_SCHEMA
} from "@angular/core";

@Component({
  selector: "router-outlet",
  template: "",
  exportAs: "RouterOutletStubComponent"
})
class RouterOutletStubComponent { }

@Directive({
  selector: "[routerLink]"
})
export class RouterLinkDirectiveStub {
  @Input("routerLink") linkParams: any;
  navigatedTo: any = null;

  @HostListener("click")
  onClick() {
    this.navigatedTo = this.linkParams;
  }
}

let comp: AppComponent;
let fixture: ComponentFixture<AppComponent>;

describe("AppComponent & TestModule", () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        RouterLinkDirectiveStub,
        RouterOutletStubComponent
      ],
      schemas: [NO_ERRORS_SCHEMA] // Avoid import NavCompotent
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(AppComponent);
        comp = fixture.componentInstance;
      });
  }));

  it("can instantiate the component", () => {
    expect(comp).toBeNull();
  });
});
