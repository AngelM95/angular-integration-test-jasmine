import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { NavComponent } from "./nav.component";
import {
  Component,
  Directive,
  Input,
  HostListener,
  DebugElement
} from "@angular/core";
import { By } from "@angular/platform-browser";

@Component({
  selector: "router-outlet",
  template: "",
  exportAs: "RouterOutletStubComponent"
})
class RouterOutletStubComponent {}

@Directive({
  selector: "[routerLink]"
})
export class RouterLinkDirectiveStub {
  @Input("routerLink") linkParams: any;
  navigatedTo: any = null;

  @HostListener("click")
  onClick() {
    this.navigatedTo = this.linkParams;
  }
}

describe("NavComponent", () => {
  let component: NavComponent;
  let fixture: ComponentFixture<NavComponent>;
  let routerLinks: RouterLinkDirectiveStub[];
  let linkDes: DebugElement[];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        NavComponent,
        RouterLinkDirectiveStub,
        RouterOutletStubComponent
      ]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(NavComponent);
        component = fixture.componentInstance;
      });
  }));

  beforeEach(() => {
    fixture.detectChanges(); // trigger initial data binding

    // find DebugElements with an attached RouterLinkStubDirective
    linkDes = fixture.debugElement.queryAll(
      By.directive(RouterLinkDirectiveStub)
    );

    // get attached link directive instances
    // using each DebugElement's injector
    routerLinks = linkDes.map(de => {
      return de.injector.get(RouterLinkDirectiveStub);
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("can get RouterLinks from template", () => {
    expect(routerLinks.length).toBe(1, "should have 1 routerLinks");
    expect(routerLinks[0].linkParams).toBe("/todos");
  });
});
