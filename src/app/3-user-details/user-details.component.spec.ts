/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { UserDetailsComponent } from "./user-details.component";
import { Router, ActivatedRoute } from "@angular/router";
import { Observable, of, Subject } from "rxjs";

describe("UserDetailsComponent", () => {
  let component: UserDetailsComponent;
  let fixture: ComponentFixture<UserDetailsComponent>;

  class RouteFake {
    navigate(params) {
      return params;
    }
  }

  class ActivatedRouteFake {
    private subject = new Subject();

    push(value) {
      this.subject.next(value);
    }

    get params() {
      return this.subject.asObservable();
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserDetailsComponent],
      providers: [
        { provide: Router, useClass: RouteFake },
        { provide: ActivatedRoute, useClass: ActivatedRouteFake }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should redirect the user to the users page after saving", () => {
    let router = TestBed.get(Router);
    let spy = spyOn(router, "navigate");
    component.save();
    expect(spy).toHaveBeenCalledWith(["users"]);
  });

  it("should navigate the user to the not found page when an invalid user id is passed", () => {
    let router = TestBed.get(Router);
    let spy = spyOn(router, "navigate");

    let route: ActivatedRouteFake = TestBed.get(ActivatedRoute);
    route.push({ id: 0 });
    expect(spy).toHaveBeenCalledWith(["not-found"]);
  });
});
