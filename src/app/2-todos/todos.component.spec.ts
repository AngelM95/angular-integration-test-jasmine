/* tslint:disable:no-unused-variable */
import {
  async,
  ComponentFixture,
  TestBed,
  tick,
  fakeAsync
} from "@angular/core/testing";

import { TodosComponent } from "./todos.component";
import { TodoService } from "./todo.service";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { of } from "rxjs";

describe("TodosComponent", () => {
  let component: TodosComponent;
  let fixture: ComponentFixture<TodosComponent>;
  let service;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [TodosComponent],
      providers: [TodoService]
    });
    fixture = TestBed.createComponent(TodosComponent);
    component = fixture.componentInstance;
    service = TestBed.get(TodoService);
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });
  // ngOnInit - include detectChanges
  it("should return todos from the server", fakeAsync(() => {
    spyOn(service, "getTodosPromise").and.returnValue(
      Promise.resolve([1, 2, 3])
    );
    fixture.detectChanges();
    // option 1 async
    /* fixture.whenStable().then(() => {
      expect(component.todos.length).toBe(3);
    }); */
    // option 2 fakeAsync
    tick();
    expect(component.todos.length).toBe(3);
  }));
});
