import { ComponentFixture, TestBed } from "@angular/core/testing";
import { By } from "@angular/platform-browser";
import { VoterComponent } from "./voter.component";

describe("VoterComponent", () => {
  let component: VoterComponent;
  let fixture: ComponentFixture<VoterComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [VoterComponent]
    });
    fixture = TestBed.createComponent(VoterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
  // Check dom element binding
  it("should render total votes", () => {
    component.othersVote = 20;
    component.myVote = 1;
    fixture.detectChanges();

    // fixture.debugElement.query(By.directive(VoterComponent))
    let debugElement = fixture.debugElement.query(By.css(".vote-count"));
    let el: HTMLElement = debugElement.nativeElement;

    expect(el.innerText).toContain("21");
  });
  // Check style element binding
  it("should highlight the upvote button if I have upvoted", () => {
    component.myVote = 1;
    fixture.detectChanges();

    let debugElement = fixture.debugElement.query(By.css(".glyphicon"));
    expect(debugElement.classes["highlighted"]).toBeTruthy();
  });
  // Check event binding
  it("should increase total votes when I click the upvote button", () => {
    let button = fixture.debugElement.query(By.css(".glyphicon"));
    button.triggerEventHandler("click", null);
    // Can use component.upVote() but that is related with unit test.
    // In beforeEach should be component = new VoteComponent();
    // In this case we want to relate it with template html
    expect(component.totalVotes).toBe(1);
  });
});
